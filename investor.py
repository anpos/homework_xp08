"""Investor."""

import csv
from collections import namedtuple


Point = namedtuple('Point', 'rate date')
Fall = namedtuple('Fall', 'start end')


def get_currency_rates_from_file(filename: str) -> tuple:
    """
    Read and return the currency and exchange rate history from file.
    See web page:
    https://www.eestipank.ee/valuutakursside-ajalugu

    Note that the return value is tuple, that consists of two things:
    1) currency name given in the file.
    2) exchange rate history for the given currency.
        Note that history is returned using dictionary where keys represent dates
        and values represent exchange rates for the dates.

    :param filename: file name to read CSV data from
    :return: Tuple that consists of currency name and dict with exchange rate history

    return ("USD", {"03.01.2017": 1.0099, "02.01.2017": 1.0101, "01.01.2017": 1.0234})
    """
    history = {}
    currency = ""
    with open(filename) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        try:
            next(csv_reader)
            next(csv_reader)
            currency = next(csv_reader)[1]
        except StopIteration:
            return currency, history
        for line in csv_reader:
            history[line[0]] = float(line[1])
    return currency, history


def exchange_money(exchange_rates: dict) -> list:
    """
    Find best dates to exchange money for maximum profit.

    You are given a dictionary where keys represent dates and values represent exchange
    rates for the dates. The amount you initially have is 1000 and you always use the
    maximum amount during the exchange.
    Be aware that there is 1% of service fee for every exchange. You only need to return
    the dates where you take action. That means the first action is always to buy the
    second currency and the second action is to sell it back. Repeat the sequence as
    many times as you need for maximum profit. You should always end up having the
    initial currency. That means there should always be an even number of actions. You can
    also decide that the best decision is to not make any transactions at all, if
    for example the rate is always dropping. In that case just return an empty list.

    :param exchange_rates: dictionary of dates and exchange rates
    :return: list of dates

    return ["01.01.2017", "03.01.2017"]
    """
    if len(exchange_rates) < 2:
        return []
    mapped = sort_history_items(exchange_rates.items())

    fall_start = Point(mapped[0][1], mapped[0][0])
    fall_end = fall_start
    falls = []
    for date, rate in mapped:
        if rate < fall_end.rate:
            fall_end = Point(rate, date)
        elif rate > fall_end.rate:
            if fall_end is not fall_start:
                falls.append(Fall(fall_start, fall_end))
            fall_start = Point(rate, date)
            fall_end = fall_start
    if fall_end is not fall_start:
        falls.append(Fall(fall_start, fall_end))
    if len(falls) == 0:
        return []

    profit_value = 1.0 / 0.9801
    chained = []
    chain = falls[0]
    for fall in falls[1:]:
        chain_value = chain.start.rate / chain.end.rate
        fall_value = fall.start.rate / fall.end.rate
        rise_value = fall.start.rate / chain.end.rate
        # continue chain
        if profit_value >= rise_value and chain_value >= rise_value and fall_value >= rise_value:
            chain = Fall(chain.start, fall.end)
        # break chain
        elif chain_value > profit_value:
            if len(chained) > 0:
                rise_value = (chain.start.rate / chained[-1].end.rate)
                if profit_value >= rise_value and chain_value >= rise_value:
                    chained[-1] = Fall(chained[-1].start, chain.end)
                    chain = fall
                    continue
            chained.append(chain)
            chain = fall
        # replace chain
        else:
            if len(chained) > 0:
                rise_value = (fall.start.rate / chained[-1].end.rate)
                if profit_value >= rise_value and fall_value >= rise_value:
                    chain = chained.pop()
                    chain = Fall(chain.start, fall.end)
                    continue
            chain = fall
    if (chain.start.rate / chain.end.rate) > profit_value:
        chained.append(chain)

    results = []
    for fall in chained:
        results.append(fall.start.date)
        results.append(fall.end.date)
    return results


def sort_history_items(items) -> list:
    """Sort this nasty thing, where first is date and second is whatever."""
    mapped = sorted(map(lambda kv: (date_to_int(kv[0]), kv[1]), items))
    return list(map(lambda kv: (int_to_date(kv[0]), kv[1]), mapped))


def date_to_int(s: str) -> int:
    """Date to integer conversion."""
    d, m, y = s.split('.', 2)
    return (int(y) << 16) | (int(m) << 8) | int(d)


def int_to_date(i: int) -> str:
    """Integer to date conversion."""
    y, m, d = (i >> 16) & 0xffff, (i >> 8) & 0xff, i & 0xff
    return f'{d:02}.{m:02}.{y:02}'
