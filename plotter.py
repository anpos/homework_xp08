"""Plotter."""

import matplotlib.pyplot as plt
from investor import *


def plot_best_profit(initial_amount: int, history_filename: str):
    """Plot best profit."""

    coefficient = 0.9801
    currency, currency_history = get_currency_rates_from_file(history_filename)
    exchange_dates = exchange_money(currency_history)

    mapped = sort_history_items(currency_history.items())
    ordered_dates, ordered_rates = zip(*mapped)

    marked1, marked2 = [], []
    growth = []
    previous_growth = 1.0
    previous_index = 0
    for n, date in enumerate(exchange_dates):
        current_index = ordered_dates.index(date)
        if (n % 2) == 0:  # exchanging into foreign currency
            marked1 += [current_index]
            growth += [previous_growth] * (current_index - previous_index)
            previous_index = current_index
        else:  # exchanging back into own currency
            marked2 += [current_index]
            growth += [(ordered_rates[previous_index] / ordered_rates[i]) * coefficient * previous_growth for i in range(previous_index, current_index + 1)]
            previous_growth = growth[-1]
            previous_index = current_index + 1
    growth += [previous_growth] * (len(ordered_dates) - previous_index)

    plot_and_show_graph(ordered_dates, ordered_rates, growth, marked1, marked2, currency, 'EUR', initial_amount)


def plot_and_show_graph(dates: list, rates: list, growth: list, marked1: list, marked2: list, currency: str, my_currency: str, amount: int):
    """Plot and show the graph using pyplot."""
    plt.style.use('seaborn')
    plt.figure(figsize=(24, 12), dpi=80)

    plt.plot(dates, growth, '-o', markevery=marked1, label=f'Balance in {my_currency}')
    plt.plot(dates, rates, '-o', markevery=marked1, label=f'{currency} rate')

    plt.scatter([dates[i] for i in marked2], [growth[i] for i in marked2], marker='^')
    plt.scatter([dates[i] for i in marked2], [rates[i] for i in marked2], marker='^')

    leg = plt.legend(frameon=True, loc='lower right')
    leg.get_frame().set_facecolor('w')
    plt.tight_layout(6.0)
    last_index = len(growth) - 1
    if last_index >= 0:
        profit = (growth[last_index] - 1.0) * amount
        percentage = (profit / amount) * 100
        plt.text(last_index, growth[last_index], f"Profit: {profit:.2f}\nFor {amount} {my_currency}\n{percentage:.2f}%")
    plt.title('INVESTMENT')
    plt.xlabel('DATES')
    plt.ylabel('BALANCE & RATE')
    if len(dates) > 60:
        plt.xticks(ticks=marked1 + marked2, rotation=90)
    else:
        plt.xticks(rotation=90)
    plt.show()


if __name__ == '__main__':
    plot_best_profit(1000, 'currency-rates_2011_2013.csv')
